# T-AIA: Travel Order Resolver Project

## Architecture of project
Project is divided in: 
- an /app folder containing modules and a main script
- a /research folder, containing our Jupyter notebook execution of our currents NLP advances
- a /test folder, containing our Unit tests  

Environment variables by using 
https://pypi.org/project/python-dotenv/
A *.env.example* file is available in the /app folder


## Install
In /research section, every packages are stated under the Jupyter notebook file

An error might happen concerning the Geograpy3 package, if it happens please install:

pip install git+https://github.com/jmbielec/geograpy3.git 

## Usage
## Examples
## Participants
Rémi Faubourg, Mathias Belcastro, Roman Merck, Alexandre Ralli
