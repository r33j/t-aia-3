import unittest
import requests


def locationToDest(destination):
    url = 'http://dev.virtualearth.net/REST/v1/Locations'
    payload = {'key': 'AnTXex8rW2h0wt8vQOErQK9y5Rh52fMcCjV1xrJDWROJeVqVHM2AYvGxkip29aHq',
               'locality': destination
               }
    r = requests.get(url, params=payload)
    return r.status_code


class TestCarCommute(unittest.TestCase):
    def test_api_qos(self):
        self.assertTrue(locationToDest("Paris") == 200)


if __name__ == '__main__':
    unittest.main()
