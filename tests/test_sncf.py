import unittest
from app import sncf_commute
import requests


def twoPointDest(postalCode1, postalCode2):
    param1 = '?from=admin:fr:' + str(postalCode1)
    param2 = '&to=admin:fr:' + str(postalCode2)
    payload = {
        'key': 'c92fd433-59ca-4042-9853-a825c1a9a64b'
    }
    url = 'https://api.sncf.com/v1/coverage/sncf/journeys' + param1 + param2

    r = requests.get(url, params=payload)
    return r.status_code


class TestSncf(unittest.TestCase):
    def test_api_answer(self):
        self.assertTrue(twoPointDest(75056, 69123) == 200)




if __name__ == '__main__':
    unittest.main()
