import unittest
import app.timetable_commute

content = app.timetable_commute


class TestTrainCommute(unittest.TestCase):
    def test_content(self):
        self.assertTrue(len(content.df) >= 0)


if __name__ == '__main__':
    unittest.main()
