# importing Request library to GET from the API.SNCF
import os
from dotenv import load_dotenv
import requests
load_dotenv()

key = os.getenv("SNCF_KEY")


def twoPointDest(postalCode1, postalCode2):
    param1 = '?from=admin:fr' + str(postalCode1)
    param2 = '&to=admin:fr' + str(postalCode2)
    payload = {
        'key': key
    }
    url = 'https://api.sncf.com/v1/coverage/sncf/journeys' + param1 + param2

    r = requests.get(url, params=payload)
    return r.json()
