import os

import pandas as pd

pd.options.display.max_columns = None
script_dir = os.path.dirname(__file__)

# first we import and use our timetable.csv
df = pd.read_csv(script_dir + '/assets/timetables.csv', sep="\t| \n", encoding='utf-8', engine='python')

df[['Départ', 'Arrivée']] = df['trajet'].str.rstrip(' - ').str.split(' - ', expand=True, n=1)
trajets = df.drop('trajet', axis='columns')


def itinerary(loc1):
    return trajets[trajets['Départ'].str.contains(str(loc1))]


def timetable(loc1, loc2):
    dataframe = trajets[trajets['Départ'].str.contains(str(loc1))]
    return dataframe[dataframe['Arrivée'].str.contains(str(loc2))]
