# importing Request library to GET from the Bing MIO API
import os
from dotenv import load_dotenv
import requests

load_dotenv()

key = os.getenv("BING_MIO_KEY")


def locationToDest(destination):
    url = 'http://dev.virtualearth.net/REST/v1/Locations'
    payload = {'key': key,
               'locality': destination
               }
    r = requests.get(url, params=payload)
    return r.text


def twoPointDest(dest1, dest2):
    url = 'http://dev.virtualearth.net/REST/V1/Routes/Driving?o=json'
    payload = {'key': key,
               'wp.0': dest1,
               'wp.1': dest2,
               'avoid': "minimizeTolls"
               }
    r = requests.get(url, params=payload)
    return r.text
