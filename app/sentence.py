import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import geograpy3


class Sentence:
    def __init__(self, audio: str, tokenized, formatted):
        self.audio = audio
        self.tokenized = tokenized
        self.formatted = formatted

    def tokenize(self):
        tokens = word_tokenize(self.audio)
        self.tokenized = nltk.pos_tag(tokens)
        return self.tokenized

    def format(self):
        stop_words = set(stopwords.words('french'))
        tokens = word_tokenize(self.audio)
        self.formatted = [w for w in tokens if w not in stop_words]
        return self.formatted

    def cities(self):
        places = geograpy3.get_place_context(text=self.audio)
        return places.cities
